/**
 * @file Utility functions to ensure data is created / destroyed properly in tests
 */
const { v4: uuidv4 } = require("uuid");

const { User } = require("../server/user");
const { Citation } = require("../server/citation");

// Helpers
const withCitation = (k) => (f) => {
  const u = new Citation({
    quote: `Quote ${k}`,
    character: `Character ${k}`,
    image: `http://example.com/image${k}`,
    characterDirection: k % 2 === 0 ? "left" : "right",
    origin: `origin${k}`,
    scores: {},
  });
  return u
    .save()
    .then(f)
    .then(() => Citation.findByIdAndDelete(u._id).exec());
};

const withUser = (k) => (f) => {
  const u = new User({ login: `TestUser${k}`, apiKey: uuidv4() });
  return u
    .save()
    .then(f)
    .then(() => User.deleteMany({ login: u.login }).exec());
};

const withCitationFromUser = (k) => (f) =>
  withUser(k)((user) =>
    withCitation(k)((citation) => {
      // eslint-disable-next-line no-param-reassign
      citation.addedBy = user.login;
      return citation.save().then((c) => f(c, user));
    })
  );

module.exports = { withCitation, withUser, withCitationFromUser };
