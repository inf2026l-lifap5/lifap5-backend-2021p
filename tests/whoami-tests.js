// Fixes UTF-8 problem in Jest, see https://stackoverflow.com/questions/49141927/express-body-parser-utf-8-error-in-test
// eslint-disable-next-line no-unused-expressions, node/no-unpublished-require
require("../node_modules/iconv-lite/encodings").encodings;

// eslint-disable-next-line node/no-unpublished-require
const request = require("supertest");
const validator = require("validator");
const { app } = require("../server/app");
const { db } = require("../server/db");

const { withUser } = require("./fixtures");

afterAll(() => {
  // Close mongoose connection after all tests are done
  // to avoid dandling handles
  db.close();
});

describe("Test de l'API whoami", () => {
  const dummyUUID = "332776a4-960c-413f-ac9c-3b17e85d0cdc";
  const wrongKey = "a-wrong-key";
  const withKey = (key) => request(app).get("/whoami").set("x-api-key", key);
  test("OK with existing key", () =>
    withUser("w1")((user) =>
      withKey(user.apiKey).expect(200, { login: user.login })
    ));
  test("Unauthorized with false key", () => withKey(dummyUUID).expect(401));
  test("Unauthorized with ill-formatted key", () =>
    withKey(wrongKey).expect(401));
  test("Unauthorized without key", () =>
    request(app).get("/whoami").expect(401));
  test("UUID validator OK", () => {
    expect(validator.isUUID(dummyUUID)).toBe(true);
  });
});
