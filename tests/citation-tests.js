// Fixes UTF-8 problem in Jest, see https://stackoverflow.com/questions/49141927/express-body-parser-utf-8-error-in-test
// eslint-disable-next-line no-unused-expressions, node/no-unpublished-require
require("../node_modules/iconv-lite/encodings").encodings;

// eslint-disable-next-line node/no-unpublished-require
const request = require("supertest");
const { app } = require("../server/app");
const { db } = require("../server/db");
const { Citation } = require("../server/citation");
const { withCitation, withUser, withCitationFromUser } = require("./fixtures");

afterAll(() => {
  // Close mongoose connection after all tests are done
  // to avoid dandling handles
  db.close();
});

describe("Citation API tests, standard cases", () => {
  test("GET on /citation is OK", () =>
    request(app)
      .get("/citations")
      .expect(200)
      .expect("Content-Type", /json/)
      .then((response) =>
        expect(response.body.length).toBeGreaterThanOrEqual(0)
      ));

  test("GET one citation", () =>
    withCitation(2)((c) =>
      request(app)
        .get(`/citations/${c._id}`)
        .expect(200)
        .expect("Content-Type", /json/)
        .then((response) => {
          const { body } = response;
          expect(body.quote).toBe(c.quote);
          return true;
        })
    ));

  test("POST one citation", () =>
    withUser(3)((u) => {
      const k = 3;
      const data = {
        quote: `Quote ${k}`,
        character: `Character ${k}`,
        image: `http://example.com/image${k}`,
        characterDirection: k % 2 === 0 ? "Left" : "Right",
        origin: `origin${k}`,
        scores: {},
      };
      return request(app)
        .post("/citations")
        .set("Content-type", "application/json")
        .set("x-api-key", u.apiKey)
        .send(data)
        .expect(201)
        .then((response) => {
          const { body } = response;
          expect(body.quote).toBe(data.quote);
          expect(body.addedBy).toBe(u.login);
          return true;
        });
    }));

  test("POST /citations/duels", () =>
    withUser(4)((u) =>
      withCitation(5)((c1) =>
        withCitation(6)((c2) => {
          const duelData = { winner: String(c1._id), looser: String(c2._id) };
          return request(app)
            .post("/citations/duels")
            .set("Content-type", "application/json")
            .set("x-api-key", u.apiKey)
            .send(duelData)
            .then((response) => {
              expect(response.statusCode).toBe(201);
              return response;
            })
            .then(() =>
              Promise.all([
                Citation.findById(c1._id).exec(),
                Citation.findById(c2._id).exec(),
              ])
            )
            .then(([nc1, nc2]) => {
              expect(nc1.scores.get(String(nc2._id)).wins).toBe(1);
              expect(nc1.scores.get(String(nc2._id)).looses).toBe(0);
              expect(nc2.scores.get(String(nc1._id)).wins).toBe(0);
              expect(nc2.scores.get(String(nc1._id)).looses).toBe(1);
              return true;
            });
        })
      )
    ));

  test("PUT on one citation", () =>
    withUser(7)((u) =>
      withCitation(8)((c) => {
        // eslint-disable-next-line no-param-reassign
        c.addedBy = u.login;
        return c
          .save()
          .then(() =>
            request(app)
              .put(`/citations/${c._id}`)
              .set("Content-type", "application/json")
              .set("x-api-key", u.apiKey)
              .send({ quote: "newQuote" })
              .expect(200)
          )
          .then((response) => {
            const { body } = response;
            expect(body.quote).toBe("newQuote");
            return true;
          })
          .then(() => Citation.findById(c._id))
          .then((c2) => {
            expect(c2.quote).toBe("newQuote");
            return true;
          });
      })
    ));
});

describe("Access control enforcement tests", () => {
  const dummyData = {
    quote: `Quote fail auth`,
    character: `Character fail auth`,
    image: `http://example.com/imagefilaAuth`,
    characterDirection: "Left",
    origin: `originfailAuth`,
  };

  test("Authentication for POST on /citations", () =>
    request(app)
      .post("/citations")
      .set("Content-type", "application/json")
      .send(dummyData)
      .expect(401));

  test("Authentication for PUT on /citations/:id", () =>
    withCitationFromUser(9)((citation) =>
      request(app)
        .put(`/citations/${citation._id}`)
        .set("Content-type", "application/json")
        .send(dummyData)
        .expect(401)
    ));

  test("Autorisation for PUT on /citations/:id", () =>
    withUser(10)((user) =>
      withCitationFromUser(11)((citation) =>
        request(app)
          .put(`/citations/${citation._id}`)
          .set("Content-type", "application/json")
          .set("x-api-key", user.apiKey)
          .expect(403)
      )
    ));

  test("Authentication on POST /citations/duels", () =>
    withCitation(11)((c1) =>
      withCitation(12)((c2) => {
        const duelData = { winner: String(c1._id), looser: String(c2._id) };
        return request(app)
          .post("/citations/duels")
          .set("Content-type", "application/json")
          .send(duelData)
          .expect(401);
      })
    ));
});

describe("Data input tests", () => {
  // Fixtures
  const dummyCitation = {
    quote: "a quote",
    character: "a character",
    origin: "an origin",
    image: "http://example.com/image",
  };
  let idx = 13;

  // Tests
  const nonEmptyFields = ["quote", "character", "origin"];

  nonEmptyFields.forEach((f) => {
    idx += 1;
    const uidx = idx;
    test(`Check for empty ${f} on POST /citations`, () =>
      withUser(uidx)((user) => {
        const newCitation = {};
        Object.assign(newCitation, dummyCitation);
        // eslint-disable-next-line security/detect-object-injection
        newCitation[f] = " ";
        return request(app)
          .post("/citations")
          .set("Content-type", "application/json")
          .set("x-api-key", user.apiKey)
          .send(newCitation)
          .expect(400);
      }));

    idx += 1;
    const ucidx = idx;
    test(`Check for non empty ${f} on PUT /citations/:id`, () =>
      withCitationFromUser(ucidx)((citation, user) => {
        const newCitation = {};
        Object.assign(newCitation, dummyCitation);
        // eslint-disable-next-line security/detect-object-injection
        newCitation[f] = " ";
        return request(app)
          .put(`/citations/${citation._id}`)
          .set("Content-type", "application/json")
          .set("x-api-key", user.apiKey)
          .send(newCitation)
          .expect(400);
      }));
  });

  const fieldsToCheck = {
    quote: "newQuote",
    character: "newCharacter",
    image: "http://example.com/newImage",
    characterDirection: "Left",
    origin: "newOrigin",
  };

  Object.keys(fieldsToCheck).forEach((f) => {
    idx += 1;
    const ucidx = idx;
    test(`Changing only ${f} on PUT /citation/:id`, () =>
      withCitationFromUser(ucidx)((citation, user) => {
        const data = {};
        // eslint-disable-next-line security/detect-object-injection
        data[f] = fieldsToCheck[f];
        return request(app)
          .put(`/citations/${citation._id}`)
          .set("Content-type", "application/json")
          .set("x-api-key", user.apiKey)
          .send(data)
          .expect(200)
          .then((response) => {
            Object.keys(fieldsToCheck).forEach((fCheck) => {
              if (String(fCheck) === String(f)) {
                // eslint-disable-next-line security/detect-object-injection
                expect(response.body[fCheck]).toBe(fieldsToCheck[fCheck]);
              } else {
                // eslint-disable-next-line security/detect-object-injection
                expect(response.body[fCheck]).toBe(citation[fCheck]);
              }
            });
            return true;
          });
      }));
  });
});
