// eslint-disable-next-line node/no-unpublished-require
const request = require("supertest");
const { app } = require("../server/app");
const { db } = require("../server/db");

afterAll(() => {
  // Close mongoose connection after all tests are done
  // to avoid dandling handles
  db.close();
});

describe("Root API test", () => {
  test("Answer on /", () => request(app).get("/").expect(200));
});
