// eslint-disable-next-line node/no-unpublished-require
// const request = require("supertest");
const { User, getUserByAPI } = require("../server/user");
const { db } = require("../server/db");
const { withUser } = require("./fixtures");

afterAll(() => {
  // Close mongoose connection after all tests are done
  // to avoid dandling handles
  db.close();
});

describe("User model tests", () => {
  test("Create and delete a user", () =>
    withUser(1)((u) => {
      expect(u.login).not.toBeUndefined();
      expect(u.apiKey).not.toBeUndefined();
      return User.findOne({ login: u.login })
        .exec()
        .then((u2) => {
          expect(u2.login).toBe(u.login);
          return u2;
        });
    }));

  test("Getting users by API key", () =>
    withUser(2)((u) =>
      getUserByAPI(u.apiKey).then((u2) => {
        expect(u2).not.toBeNull();
        expect(u2.login).toBe(u.login);
        return u2;
      })
    ));

  test("Not getting inexistent user", () =>
    getUserByAPI("DoesNotExist").then((u) => expect(u).toBeNull()));
});
