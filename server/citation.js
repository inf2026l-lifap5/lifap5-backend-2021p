/**
 * @file Modèle pour les citations et fonctions associées
 * Basé en partie sur https://github.com/JLuboff/TheSimpsonsQuoteAPI
 * @author Emmanuel COQUERY
 */

const { Router } = require("express");
const mongoose = require("mongoose");
const { logger } = require("./logger");

const { authByApiKey } = require("./generic-handlers");

const citationSchema = new mongoose.Schema({
  quote: String,
  character: String,
  image: String,
  characterDirection: String,
  origin: String,
  addedBy: String,
  scores: { type: Map, of: { wins: Number, looses: Number } },
});

const citationUpdatableFields = [
  "quote",
  "character",
  "image",
  "characterDirection",
  "origin",
];

/**
 * Filter the fields of an input data.
 * @param {Object} inputData the input data from a post/put
 * @returns The data, keeping only required fields
 */
const filterInputFields = (inputData) => {
  const result = {};
  citationUpdatableFields
    .filter((f) => Object.prototype.hasOwnProperty.call(inputData, f))
    .forEach((f) => {
      // eslint-disable-next-line no-param-reassign, security/detect-object-injection
      result[f] = inputData[f];
    });
  return result;
};

const citationNonEmptyFields = ["quote", "origin", "character"];
const checkFields = (data) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const f of citationNonEmptyFields) {
    // eslint-disable-next-line no-prototype-builtins, security/detect-object-injection
    if (data.hasOwnProperty(f) && data[f].trim() === "") {
      return `Field '${f}' cannot be empty`;
    }
  }
  return false;
};

/**
 * Calcule une nouveau score en fonction de la viectoire ou de la défaite.
 *
 * @param {Score} score le nombre de victoires et de défaites
 * @param {boolean} hasWon si il faut ajouter une victoire (true) ou une défaite (false)
 * @returns Le score mis à jour
 */
const incScore = (score, hasWon) =>
  hasWon
    ? { wins: score.wins + 1, looses: score.looses }
    : { wins: score.wins, looses: score.looses + 1 };

/**
 * Mets à jour la map des score en fonction du résultat d'un duel avec une autre citation.
 *
 * @param {String | ObjectId} autreCitationId l'autre citation lors d'une mise à jour de score
 * @param {*} hasWon si la citation modifiée a gagné ou perdu
 */
citationSchema.methods.majScore = function majScore(autreCitationId, hasWon) {
  if (this.scores === undefined) {
    this.scores = {};
  }
  const key = String(autreCitationId);
  if (this.scores.get(key) === undefined) {
    this.scores.set(key, { wins: 0, looses: 0 });
  }
  this.scores.set(key, incScore(this.scores.get(key), hasWon));
};

const Citation = mongoose.model("Citation", citationSchema);

/**
 * Récupères les informations sur une citation via son id Mongo
 * @param {String} id l'identifiant
 * @returns la citation ou null si elle n'existe pas.
 */
const getCitation = (id) => Citation.findById(id).exec();

/**
 * Créée une citation et la sauvegarde en base.
 * @param {JSON} data Les données de la citation
 * @returns la promesse de la citation créée
 */
const creerCitation = (data) => {
  const citation = Citation(data);
  return citation.save();
};

/**
 * Mets à jour deux citations après un duel.
 * @param {String} idWin identifiant de la citation gagnante
 * @param {String} idLoose identifiant de la citation perdante
 * @returns les deux citations mises à jour dans un tableau
 */
const duel = (idWin, idLoose) =>
  Promise.all([getCitation(idWin), getCitation(idLoose)])
    .then(([win, loose]) => {
      logger.debug(`Duel, winner: ${idWin}, looser: ${idLoose}`);
      if (!win) throw new Error(`citation ${idWin} inconnue`);
      else if (!loose) throw new Error(`citation ${idLoose} inconnue`);
      else return [win, loose];
    })
    .then(([win, loose]) => {
      logger.debug(`Duel maj scores, winner: ${win._id}, looser: ${loose._id}`);
      win.majScore(loose._id, true);
      loose.majScore(win._id, false);
      logger.debug(`Duel: maj ok`);
      return [win, loose];
    })
    .then((citations) => Promise.all(citations.map((x) => x.save())));

/**
 * Handles get requests for getting all citations.
 * @param {Request} req the http request
 * @param {Response} res the http response
 * @param {*} _next the functionto call to continue the handling chain
 * @returns The promise of managing the response
 */
const allCitationsHandler = (req, res, _next) =>
  Citation.find()
    .exec()
    .then((l) => res.json(l));

/**
 * Returns one citation by its id
 * @param {Request} req the http request
 * @param {Response} res the http response
 * @returns The promise managing the response
 */
const oneCitationHandler = (req, res) => {
  logger.debug(`looking for citation '${req.param.id}'`);
  return Citation.findById(req.params.id)
    .exec()
    .then((citation) =>
      citation
        ? res.json(citation)
        : res
            .status(404)
            .json({ status: 404, message: `citation ${req.param.id} inconnue` })
    );
};

/**
 * Gère une requête de vote (duel).
 * @param {Request} req la requête
 * @param {Response} res la réponse
 */
const duelHandler = (req, res) => {
  const { winner, looser } = req.body;
  duel(winner, looser)
    .then((citations) => res.status(201).json(citations))
    .catch((err) =>
      res.status(400).json({ status: 400, message: err.message })
    );
};

/**
 * Gère la requête POST qui demande la création d'une citation.
 * @param {Request} req la requete
 * @param {Response} res la réponse
 */
const postCitationHandler = (req, res) => {
  const data = filterInputFields(req.body);
  const checkResult = checkFields(data);
  if (checkResult) {
    return res.status(400).json({ status: 400, message: checkResult });
  }
  data.addedBy = res.locals.user.login;
  return creerCitation(data)
    .then((citation) => {
      logger.info(
        `Création de la citation ${citation._id} par ${citation.addedBy}`
      );
      return citation;
    })
    .then((citation) => res.status(201).json(citation))
    .catch((err) =>
      res.status(400).json({ status: 400, message: err.message })
    );
};

/**
 * Gère une requête de mise à jour de citation
 * @param {Request} req la requête
 * @param {Response} res la réponse
 */
const putCitationHandler = (req, res) =>
  getCitation(req.params.id).then((citation) => {
    if (!citation) {
      return res
        .status(404)
        .json({ status: 404, message: `citation ${req.param.id} inconnue` });
    }
    if (citation.addedBy !== res.locals.user.login) {
      return res.status(403).json({
        status: 403,
        message: `modification interdite: la citation ${citation._id} n'a pas été créée par ${res.locals.user.login}`,
      });
    }
    const filteredFields = filterInputFields(req.body);
    const checkResult = checkFields(filteredFields);
    if (checkResult) {
      return res.status(400).json({ status: 400, message: checkResult });
    }
    Object.keys(filteredFields).forEach((f) => {
      // eslint-disable-next-line security/detect-object-injection, no-param-reassign
      citation[f] = filteredFields[f];
    });
    // eslint-disable-next-line promise/no-nesting
    return citation.save().then((c2) => res.status(200).json(c2));
  });

/**
 * Créer un routeur pour les citations.
 *
 * Ajoute la gestion de la variable id dans le path.
 *
 * @param {Express} app l'application Express
 * @returns un routeur pour les citations
 */
const citationRouter = (app) => {
  app.param("id", (req, res, next, id) => {
    req.params.id = id;
    next();
  });

  const router = Router();

  router.get("/", allCitationsHandler);
  router.get("/:id", oneCitationHandler);
  router.put("/:id", [authByApiKey, putCitationHandler]);
  router.post("/duels", [authByApiKey, duelHandler]);
  router.post("/", [authByApiKey, postCitationHandler]);
  return router;
};

module.exports = { Citation, getCitation, router: citationRouter };
