/**
 * @file Web application definition
 * @author Emmanuel COQUERY
 */

// Libraries
const express = require("express");
const xss = require("xss-clean");
const cors = require("cors");
const path = require("path");

// App's modules
/* const db = */ require("./db");
const citation = require("./citation");
const whoami = require("./whoami");
const apiDocsRouter = require("./apiDocsRouter");

const app = express();

// Allow cross-origin requests
app.use(cors({ origin: "*" }));

app.use("/", express.static(path.join(__dirname, "../static")));

app.get("/", (req, res) => {
  res.set("Location", "/index.html");
  res.status(308);
  res.send(`<html><body>Si vous n'êtes pas redirigé, 
  allez ici: <a href="/index.html">/index.html</a></body></html>`);
});

// body-parser
app.use(express.json());

// Sanitize input data
app.use(xss());

// router for swagger-ui on swagger-jsdoc
app.use("/api-docs", apiDocsRouter(app));

app.use("/whoami", whoami.router(app));
app.use("/citations", citation.router(app));

module.exports = { app };
