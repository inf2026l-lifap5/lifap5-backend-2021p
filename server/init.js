/**
 * @file Code lancé indépendament afin d'initialiser le contenu de la base Mongo
 * @author Emmanuel COQUERY
 */
const fs = require("fs");
const util = require("util");
const csv = require("csv");
const yargs = require("yargs");
const uuid = require("uuid");
const { Citation } = require("./citation");
const { User } = require("./user");
const { db } = require("./db");
const { logger } = require("./logger");

/**
 * Loads citations into the mongo db.
 * @param {String} filename the name of the file containing the data
 * @param {Boolean} emptyFirst  whether to emtpy the collection first
 * @returns The promise of loaded citations
 */
const loadCitations = (filename, emptyFirst) => {
  let initP;
  if (emptyFirst) {
    initP = Citation.deleteMany({})
      .exec()
      .then(() => logger.info("deleted citations"));
  } else {
    initP = Promise.resolve(null);
  }
  return (
    initP
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      .then(() => util.promisify(fs.readFile)(filename))
      .then((buff) => buff.toString())
      .then(JSON.parse)
      .then((data) =>
        Promise.all(
          data.map((citationJson) => new Citation(citationJson).save())
        )
      )
      .then((inserted) => logger.info(`inserted ${inserted.length} citations`))
  );
};

function readTsv(filename) {
  return new Promise((resolve, reject) => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.readFile(filename, (err, data) => {
      if (err) reject(err);
      else {
        const input = data.toString();
        csv.parse(input, { delimiter: "\t" }, (errcsv, datacsv) => {
          if (errcsv) reject(errcsv);
          else resolve(datacsv);
        });
      }
    });
  });
}

function loadOneUser(row) {
  const apiKey = row[1] || uuid.v4();
  return new User({ login: row[0], apiKey }).save();
}

function loadUsers(filename, emptyFirst) {
  const initP = emptyFirst
    ? User.deleteMany({})
        .exec()
        .then(() => logger.info("deleted users"))
    : Promise.resolve();
  return initP
    .then(() => readTsv(filename))
    .then((data) => Promise.all(data.map(loadOneUser)))
    .then((inserted) => logger.info(`inserted ${inserted.length} users`));
}

function run(argv) {
  const citationLoad = argv.citations
    ? loadCitations(argv.citations, Boolean(argv.delete))
    : Promise.resolve(undefined);
  const userLoad = argv.users
    ? loadUsers(argv.users, Boolean(argv.delete))
    : Promise.resolve(undefined);

  return Promise.all([citationLoad, userLoad]).finally(() => db.close());
}

/**
 * Result of command-line argument parsing
 */
const { argv } = yargs
  .option("users", {
    description: "Fichier tsv des utilisateurs et des clés d'API",
    type: "string",
  })
  .option("citations", {
    description: "Fichier JSON de citations à charger",
    type: "string",
  })
  .option("delete", {
    description: "Vide les collections concernées avant d'insérer les données",
    type: "boolean",
  })
  .help()
  .alias("help", "h");

run(argv);
