/**
 * @file route whoami pour confirmer l'authentification
 * @author Emmanuel COQUERY
 */

const { Router } = require("express");
const { authByApiKey } = require("./generic-handlers");

/**
 * Gère les requêtes whoami.
 * @param {Request} req
 * @param {Response} res
 */
const whoamiHandler = (req, res) => {
  const userInfos = { login: res.locals.user.login };
  return res.status(200).json(userInfos);
};

/**
 * Créée un router pour les requêtes sur /whoami
 * @param {Express} _app the express application
 * @returns the router for whoami
 */
const whoamiRouter = (_app) => {
  const router = Router();
  router.get("/", [authByApiKey, whoamiHandler]);
  return router;
};
module.exports = { router: whoamiRouter };
