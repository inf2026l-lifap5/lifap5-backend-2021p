/**
 * @file db.js sets up and connects to the MongoDB database through Mongoose
 */

// Libraries
const mongoose = require("mongoose");

// App's modules
const config = require("./config");
const { logger } = require("./logger");

mongoose.connect(config.mongodbUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", (err) => logger.error(`MongoDB error: ${err}`));
db.once("open", () => {
  logger.info("Connected to Mongo");
});

module.exports = { db };
